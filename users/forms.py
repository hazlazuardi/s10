from django import forms
# from django.utils.timezone import now
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)

class UserForm(forms.Form):
    username = forms.CharField(
        max_length=20,
        widget=forms.TextInput(attrs={
            'class':'inputText',
            'placeholder': 'Unique username'
            })
        )
    password = forms.CharField(
        max_length=20, 
        widget=forms.PasswordInput(attrs={
            'class':'inputText',
            'placeholder': 'Your highly classified password'
            })
        )

    class Meta:
        model = User
        fields = ['username','password']


class UserCreateForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False)
    last_name = forms.CharField(max_length=150, required=False)
    email = forms.EmailField(max_length=100, required=False)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', ]

