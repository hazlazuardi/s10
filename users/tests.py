from django.test import TestCase

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time
import random

from django.test import Client
from django.urls import resolve

from .views import index, django_login,django_signup

from django.contrib.auth.models import User


class S10UnitTest(TestCase):

# URL
    def test_s10_url_dashboard_havent_login_yet_is_exist(self):
        response = Client().get('/?next=/dashboard/')
        self.assertEqual(response.status_code, 200)

    def test_s10_url_django_account_index_is_exist(self):
        response = Client().get('/djangoaccounts/')
        self.assertEqual(response.status_code, 200)

    def test_s10_url_django_account_signup_is_exist(self):
        response = Client().get('/djangoaccounts/signup/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('djaccsignup.html')

    def test_s10_url_django_account_signup_post_is_exist(self):
        response = Client().post('/djangoaccounts/signup/', {'username':'hazduar', 'password1':"123", 'password2':"123"})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('djaccsignup.html')
   
    def test_s10_url_django_account_signup_post_is_exist_then_redirect_to_login(self):
        response = Client().post('/djangoaccounts/signup/', {'username':'dididaduar', 'password1':"w4es2msd", 'password2':"w4es2msd"})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('djacclogin.html')

    def test_s10_url_django_account_signup_post_is_exist_but_password_doesnt_match(self):
        response = Client().post('/djangoaccounts/signup/', {'username':'123', 'password1':"123", 'password2':"321"})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('djaccsignup.html')

    def test_s10_url_django_account_login_get_is_exist(self):
        response = Client().get('/djangoaccounts/login/')
        self.assertEqual(response.status_code, 200)

    def test_s10_url_django_account_login_post_is_exist(self):
        response = Client().post('/djangoaccounts/login/', {'username':'hazduar', 'password':"hazduar"})
        self.assertEqual(response.status_code, 200)
    
# Functions
    def test_s10_index_function(self):
        found = resolve('/djangoaccounts/')
        self.assertEqual(found.func, index)

    def test_s10_signup_function(self):
        found = resolve('/djangoaccounts/signup/')
        self.assertEqual(found.func, django_signup)

    def test_s10_login_function(self):
        found = resolve('/djangoaccounts/login/')
        self.assertEqual(found.func, django_login)

    def test_s10_model(self):
        User.objects.create(username='hazduar', password='123')
        count = User.objects.all().count()
        self.assertEqual(count, 1)

url = 'http://story10haz.herokuapp.com'

class S10FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(S10FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(S10FunctionalTest, self).tearDown()

    def test_home(self):
        selenium = self.selenium
        selenium.get(url)
        title = selenium.find_element_by_css_selector('.greeting > h3')
        strangerGreeting = title.get_attribute('innerHTML') == "Hello there, have I seen you before?"
        self.assertTrue(strangerGreeting)

    def test_djangoSignup_redirectto_djangoLogin_redirectto_dashboard_checkCookieTodjangoaccounts_todashboard_ejectbyLogout(self):
        selenium = self.selenium
        selenium.get(url+'/djangoaccounts/signup/')
        time.sleep(3)

        # Find the form
        username = selenium.find_element_by_id('id_username')
        password1 = selenium.find_element_by_id('id_password1')
        password2 = selenium.find_element_by_id('id_password2')

        # Username is unique, handle the already created accounts by using random number
        post = str(random.randint(1000, 10000))

        # Fill the form, not valid
        username.send_keys("hazlazuardi"+post) 
        password1.send_keys('w3w1duararua')
        password2.send_keys('w3w1duararua')
        # Submit, create account
        createaccountbtn = selenium.find_element_by_css_selector('.createaccount')
        createaccountbtn.click()

        # Should be redirected to sign-up path for login
        # already auto-completed, hence, should only need to click loginbtn
        time.sleep(3)
        loginbtn = selenium.find_element_by_css_selector('.loginbtn')
        loginbtn.click()

        # Should be redirected to dashboard
        selenium.implicitly_wait(2)
        title = selenium.find_element_by_css_selector('.welcome')
        memberGreeting = title.get_attribute('innerHTML') == "Welcome back, hazlazuardi"+post+" !"
        self.assertTrue(memberGreeting)

        # Check Cookie, going to Django Account page
        selenium.get(url+'/djangoaccounts/')
        selenium.implicitly_wait(1)
        title = selenium.find_element_by_css_selector('.welcome')
        memberGreeting = title.get_attribute('innerHTML') == "Welcome back, " + 'hazlazuardi'+post+' !'
        self.assertTrue(memberGreeting)


        # redirect to dashboard
        redirectToDashboard = selenium.find_element_by_css_selector('.btn')
        redirectToDashboard.click()

        # Logout from dashboard
        selenium.implicitly_wait(2)
        logout = selenium.find_element_by_css_selector('.logoutbtn')
        logout.click()

        # Back to Get Started
        title = selenium.find_element_by_css_selector('.greeting > h3')
        strangerGreeting = title.get_attribute('innerHTML') == "Hello there, have I seen you before?"
        self.assertTrue(strangerGreeting)


# Selenium Progress (can't find accordion header)
# [08/May/2020 04:42:44] "GET / HTTP/1.1" 200 727
# [08/May/2020 04:42:44] "GET /static/css/main.css HTTP/1.1" 200 6523
# [08/May/2020 04:42:46] "GET /djangoaccounts/signup/ HTTP/1.1" 200 2118
# [08/May/2020 04:42:46] "GET /static/css/main.css HTTP/1.1" 200 6523
# [08/May/2020 04:42:49] "POST /djangoaccounts/signup/ HTTP/1.1" 200 1450
# [08/May/2020 04:42:52] "POST /djangoaccounts/login/ HTTP/1.1" 302 0
# [08/May/2020 04:42:52] "GET /dashboard/ HTTP/1.1" 200 489
# [08/May/2020 04:42:52] "GET /djangoaccounts/ HTTP/1.1" 200 779
# [08/May/2020 04:42:52] "GET /dashboard/ HTTP/1.1" 200 489

# Selenium Progress (OK)
# [08/May/2020 04:47:22] "GET / HTTP/1.1" 200 727
# [08/May/2020 04:47:22] "GET /static/css/main.css HTTP/1.1" 200 6523
# [08/May/2020 04:47:23] "GET /djangoaccounts/signup/ HTTP/1.1" 200 2118
# [08/May/2020 04:47:24] "GET /static/css/main.css HTTP/1.1" 200 6523
# [08/May/2020 04:47:27] "POST /djangoaccounts/signup/ HTTP/1.1" 200 1450
# [08/May/2020 04:47:30] "POST /djangoaccounts/login/ HTTP/1.1" 302 0
# [08/May/2020 04:47:30] "GET /dashboard/ HTTP/1.1" 200 498
# [08/May/2020 04:47:30] "GET /djangoaccounts/ HTTP/1.1" 200 788
# [08/May/2020 04:47:30] "GET /dashboard/ HTTP/1.1" 200 498
# [08/May/2020 04:47:30] "GET /logout/ HTTP/1.1" 302 0
# [08/May/2020 04:47:30] "GET / HTTP/1.1" 200 727

# Man Hand
# [08/May/2020 04:34:59] "POST /djangoaccounts/signup/ HTTP/1.1" 200 1431
# [08/May/2020 04:35:02] "POST /djangoaccounts/login/ HTTP/1.1" 302 0
# [08/May/2020 04:35:02] "GET /dashboard/ HTTP/1.1" 200 470
# [08/May/2020 04:37:52] "GET /logout/ HTTP/1.1" 302 0
# [08/May/2020 04:37:52] "GET / HTTP/1.1" 200 727
# [08/May/2020 04:37:52] "GET /static/css/main.css HTTP/1.1" 304 0