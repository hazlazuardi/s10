from django.shortcuts import redirect, render, reverse
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .forms import UserForm, UserCreateForm
from django.contrib import messages
from django.shortcuts import redirect, render, reverse
from django.http import HttpResponse


def index(request):
    if request.method == "GET":
        return render(request, "users/djaccindex.html")
        
def django_signup(request):
    f = UserCreateForm(request.POST)
    context = {
        'signupform': f
    }
    if request.method == "GET":
        return render(request, "users/djaccsignup.html", context)
    if request.method == 'POST':
        if f.is_valid():
            f.save()
            username = request.POST['username']
            password1 = request.POST['password1']
            context = {
                'username': username,
                'password': password1
            }
            return render(request, "users/djacclogin.html", context) 
    else:
        f = UserCreateForm()
 
    return render(request, 'users/djaccsignup.html', context)
    
    # context = {
    #     'signupform': CustomUserCreationForm()
    # }
    # if request.method == "GET":
    #     return render(request, "users/djaccsignup.html", context)
    # if request.method == 'POST':
    #     password1 = request.POST['password1']
    #     password2 = request.POST['password2']
    #     if password1 != password2:
    #         context['error'] = "Please check your typing, it doesn't match."
    #         return render(request, "users/djaccsignup.html", context)
    #     else:
    #         username = request.POST['username']
    #         user = User.objects.create_user(username=username, password=password1)
    #         context = {
    #             'username': user.username,
    #             'password': password1
    #         }
    #         return render(request, "users/djacclogin.html", context)

def django_login(request):            
    context = {
        'loginform': UserForm()
    }
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if(user is not None):
            request.session['username'] = username
            login(request, user)
            request.method = "GET"
            return redirect(reverse('dashboard:index'))
        else:
            return render(request, "users/djacclogin.html", context)
    return render(request, "users/djacclogin.html", context)