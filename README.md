
# Story 10
This Gitlab repository is the result of the work from **Wishnu Hazmi**

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/hazlazuardi/s10/badges/master/pipeline.svg)](https://gitlab.com/hazlazuardi/s10/commits/master)
[![coverage report](https://gitlab.com/hazlazuardi/s10/badges/master/coverage.svg)](https://gitlab.com/hazlazuardi/s10/commits/master)

## URL
This story can be accessed from [https://story10haz.herokuapp.com](https://story10haz.herokuapp.com)

## Author
**Wishnu Hazmi** - [hazlazuardi](https://gitlab.com/hazlazuardi)